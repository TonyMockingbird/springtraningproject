package tony.dao;

import java.util.List;

import tony.form.Contact;

public interface ContactDAO {

    public void addContact(Contact contact);
    public List<Contact> listContact();
    public void removeContact(Integer id);
}
