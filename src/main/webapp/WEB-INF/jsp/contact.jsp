<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
    <title>Spring 3 MVC </title>
</head>
<body>

<h2>Contact Manager</h2>

<form:form method="post" action="add.html" commandName="contact">

    <table>
        <tr>
            <td><label>FirstName</label></td>
            <td><form:input path="firstname" /></td>
        </tr>
        <tr>
            <td><label>LastName</label></td>
            <td><form:input path="lastname" /></td>
        </tr>
        <tr>
            <td><label>Email</label></td>
            <td><form:input path="email" /></td>
        </tr>
        <tr>
            <td><label>Telephone</label></td>
            <td><form:input path="telephone" /></td>
        </tr>
        <tr>
            <td colspan="2">
                <input type="submit" value="Add contact"/>
            </td>
        </tr>
    </table>
</form:form>


<h3>Contacts</h3>
<c:if  test="${!empty contactList}">
    <table class="data">
        <tr>
            <th>Name</th>
            <th>Email</th>
            <th>Telephone</th>
            <th>&nbsp;</th>
        </tr>
        <c:forEach items="${contactList}" var="contact">
            <tr>
                <td>${contact.lastname}, ${contact.firstname} </td>
                <td>${contact.email}</td>
                <td>${contact.telephone}</td>
                <td><a href="delete/${contact.id}">delete</a></td>
            </tr>
        </c:forEach>
    </table>
</c:if>

</body>
</html>